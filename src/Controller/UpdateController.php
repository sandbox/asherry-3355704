<?php

namespace Drupal\update_qa\Controller;

use Drupal\update\Controller\UpdateController as UpdateControllerCore;
use Drupal\update\UpdateManagerInterface;
use Drupal\update\UpdateFetcherInterface;

/**
 * Returns responses for Update QA routes.
 */
class UpdateController extends UpdateControllerCore {

  /**
   * Builds the response.
   */
  public function updateStatus() {

    $update_status = parent::updateStatus();
    $build = [];

    $data = isset($update_status['#data']) && is_array($update_status['#data']) ? $update_status['#data'] : [];

    $last = \Drupal::state()->get('update.last_check', 0);

    $build['last_checked'] = [
      '#theme' => 'update_last_check',
      '#last' => $last,
      // Attach the library to a variable that gets printed always.
      '#attached' => [
        'library' => [
          'update/drupal.update.admin',
        ],
      ],
    ];

    // For no project update data, populate no data message.
    if (empty($data)) {
      $build['no_updates_message'] = _update_no_data();
    }

    $build['table'] = [
      '#header' => ['Title', 'Type', 'Current version', 'Recommended'],
      '#type' => 'table',
      '#rows' => [],
    ];
    $rows = &$build['table']['#rows'];

    foreach ($data as $project) {

      $status = $project['status'];
      if ($status === UpdateManagerInterface::CURRENT) {
        continue;
      }

      $row_key = !empty($project['title']) ? mb_strtolower($project['title']) : mb_strtolower($project['name']);
      $recommended_version = $project['recommended'];
      $security_updates = !empty($project['security updates']) ? array_column($project['security updates'], 'version') : [];
      if (in_array($recommended_version, $security_updates)) {
        $recommended_version .= " (Security update)";
      }

      $cols = [
        $project['title'],
        $project['project_type'],
        $project['existing_version'],
        $recommended_version,
      ];

//      // Add project status class attribute to the table row.
//      switch ($project['status']) {
//        case UpdateManagerInterface::CURRENT:
//          $rows[$project['project_type']][$row_key]['#attributes'] = ['class' => ['color-success']];
//          break;
//
//        case UpdateFetcherInterface::UNKNOWN:
//        case UpdateFetcherInterface::FETCH_PENDING:
//        case UpdateFetcherInterface::NOT_FETCHED:
//        case UpdateManagerInterface::NOT_SECURE:
//        case UpdateManagerInterface::REVOKED:
//        case UpdateManagerInterface::NOT_SUPPORTED:
//          $rows[$project['project_type']][$row_key]['#attributes'] = ['class' => ['color-error']];
//          break;
//
//        case UpdateFetcherInterface::NOT_CHECKED:
//        case UpdateManagerInterface::NOT_CURRENT:
//        default:
//          $rows[$project['project_type']][$row_key]['#attributes'] = ['class' => ['color-warning']];
//          break;
//      }

      $rows[] = $cols;
    }


    $project_types = [
      'core' => t('Drupal core'),
      'module' => t('Modules'),
      'theme' => t('Themes'),
      'module-disabled' => t('Uninstalled modules'),
      'theme-disabled' => t('Uninstalled themes'),
    ];

    $variables['project_types'] = [];
    foreach ($project_types as $type_name => $type_label) {
      if (!empty($rows[$type_name])) {
        ksort($rows[$type_name]);
        $variables['project_types'][] = [
          'label' => $type_label,
          'table' => $rows[$type_name],
        ];
      }
    }

    return $build;
  }

}
